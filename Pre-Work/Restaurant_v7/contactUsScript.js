function checkForm()
{
	var note = "Please fill in the following information:"
	var popup = false; //Only display a pop-up if the form is missing some necessary information
	
	//If the name field has no value...
	if (!document.getElementById("name").value)
	{
		note += "\nYour name";
		popup = true;
	}
	//If both the email and name field have no value...
	if (!document.getElementById("email").value && !document.getElementById("phone").value)
	{
		note += "\nYou email and/or phone number";
		popup = true;
	}
	//If "Other" is selected, and the "additional info" field has no value...
	if (document.getElementById("reason").value == "Other" && !document.getElementById("additionalInfo").value)
	{
		note += "\nAdditional information about why you are contacting us"
		popup = true;
	}
	
	/*Evaluate checkboxes*/
	var mon = document.getElementById("mon").checked;
	var tues = document.getElementById("tues").checked;
	var wed = document.getElementById("wed").checked;
	var thurs = document.getElementById("thurs").checked;
	var fri = document.getElementById("fri").checked;
	var anyChecked = (mon || tues || wed || thurs || fri); //Are any of the checkboxes checked?
	
	// If none of the checkboxes are checked...
	if (!anyChecked)
	{
		note += "\nThe best day(s) for us to contact you";
		popup = true;
	}
	
	//If the pop-up message needs displayed...
	if (popup)
	{
		window.alert(note); //Display the pop-up message
		return false; //return false so that the form does not submit
	}
	
	return true; //If all the necessary information is there, then submit the form
}