function play()
{
	/*
		Get the starting bet,
		round it down to the nearest dollar,
		and append a ".00" to the string
	*/
	var startAmt = document.getElementById("startAmtInput").value;
	startAmt = Math.floor(startAmt);
	startAmt += ".00";
	document.getElementById("startAmtInput").value = startAmt;
	
	var curAmt = startAmt; //Initiate the current amount of money as the starting amount
	var rollCount = 0;
	var highAmt = startAmt; //Initiate the highest amount owned as the starting amount
	var highAmtRollCount = 0;
	
	while (curAmt > 0)
	{
		var roll = rollDice();
		rollCount++;
		if (roll == 7)
		{
			curAmt += 4;
		}
		else
		{
			curAmt -= 1;
		}
		
		//Compare current amount of money to highest achieved amount of money
		//Update the highest achieved amount and corresponding roll count if necessary
		if (curAmt > highAmt)
		{
			highAmt = curAmt;
			highAmtRollCount = rollCount;
		}
	}
	
	//Round down highAmt to nearest dollar, and append ".00"
	highAmt = Math.floor(highAmt);
	highAmt += ".00";
	
	//Display results in table
	document.getElementById("tableDiv").style.display = "block";
	document.getElementById("startAmtCell").innerHTML = "$"+startAmt;
	document.getElementById("rollCountCell").innerHTML = rollCount;
	document.getElementById("highAmtCell").innerHTML = "$"+highAmt;
	document.getElementById("highAmtRollCountCell").innerHTML = highAmtRollCount;
	
	//Change the button's text to prompt the player to play again
	document.getElementById("playButton").innerHTML = "Play Again";
}

//Simulates rolling two dice, and returns the result
function rollDice()
{
	//Randomly generate two integers between 1 and 6
	var dice1 = Math.ceil(Math.random()*6);
	var dice2 = Math.ceil(Math.random()*6);
	
	//In case Math.random() generates a 0 (extremely rare), round it up to 1
	if (dice1 == 0)
	{
		dice1 = 1;
	}
	if (dice2 == 0)
	{
		dice2 = 1;
	}
	
	//Add the sum of the two dice results
	return dice1 + dice2;
}